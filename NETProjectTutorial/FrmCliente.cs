﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {

        private DataSet dsCliente;
        private DataTable dtCliente;
        private DataRow drCliente;
        private BindingSource bsCliente;

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
                txtCedula.Text = drCliente["Cédula"].ToString();
                txtNames.Text = drCliente["Nombres"].ToString();
                txtLNames.Text = drCliente["Apellidos"].ToString();
                txtTel.Text = drCliente["Teléfono"].ToString();
                txtMail.Text = drCliente["Correo"].ToString();
                txtDireccion.Text = drCliente["Dirección"].ToString();
            }

        }

        public DataSet DsCliente
        {
            get
            {
                return dsCliente;
            }

            set
            {
                dsCliente = value;
            }
        }

        public DataTable DtCliente
        {
            get
            {
                return dtCliente;
            }

            set
            {
                dtCliente = value;
            }
        }

        public FrmCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string ced, name, lname, tel, mail, dir;


            try
            {
                ced = txtCedula.Text;
                name = txtNames.Text;
                lname = txtLNames.Text;
                tel = txtTel.Text;
                mail = txtMail.Text;
                dir = txtDireccion.Text;

            }
            catch (FormatException)
            {
                MessageBox.Show("ERROR, debe llenar los datos que se le pide para poder agregar el cliente", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (name.Equals("") || lname.Equals("") || tel.Equals(""))
            {
                MessageBox.Show("Debe ingresar al menos Nombre, Apellido y Teléfono", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (drCliente != null)
            {
                DataRow New = dtCliente.NewRow();

                int indice = DtCliente.Rows.IndexOf(drCliente);

                New["Id"] = drCliente["Id"];
                New["Cédula"] = ced;
                New["Nombres"] = name;
                New["Apellidos"] = lname;
                New["Teléfono"] = tel;
                New["Correo"] = mail;
                New["Dirección"] = dir;

                DtCliente.Rows.RemoveAt(indice);
                DtCliente.Rows.InsertAt(New, indice);

            }
            else
            {
                DtCliente.Rows.Add(DtCliente.Rows.Count + 1, ced, name, lname, tel, mail, dir);
            }
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = DsCliente;
            bsCliente.DataMember = DsCliente.Tables["Clientes"].TableName;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            //uoiuuio
        }
    }
}
