﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionCliente : Form
    {

        private DataSet dsClientes;
        private BindingSource bsClientes;

        public DataSet DsClientes
        {
            get
            {
                return dsClientes;
            }

            set
            {
                dsClientes = value;
            }
        }

        public FrmGestionCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.DtCliente = DsClientes.Tables["Clientes"];
            fc.DsCliente = dsClientes;
            fc.ShowDialog();
        }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = DsClientes;
            bsClientes.DataMember = DsClientes.Tables["Clientes"].TableName;
            dgvClientes.DataSource = bsClientes;
            dgvClientes.AutoGenerateColumns = true; 
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvClientes.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show("ERROR, debe seleccionar una fila de la tabla para poder realizar cambios pertinentes", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gView = rowCollection[0];
            DataRow dRow = ((DataRowView)gView.DataBoundItem).Row;

            FrmCliente fc = new FrmCliente();
            fc.DtCliente = DsClientes.Tables["Clientes"];
            fc.DsCliente = DsClientes;
            fc.DrCliente = dRow;
            fc.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsClientes.Filter = string.Format("Nombres like '*{0}*' or Apellidos like '*{0}*' or Teléfono like '*{0}*'", textBox1.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rCol = dgvClientes.SelectedRows;

            if (rCol.Count == 0)
            {
                MessageBox.Show("ERROR, debe seleccionar una fila de la tabla para poder realizar cambios pertinentes", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gRow = rCol[0];
            DataRow drow = ((DataRowView)gRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "¿Realmente desea eliminar este cliente?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DsClientes.Tables["Clientes"].Rows.Remove(drow);
                MessageBox.Show(this, "¡Cliente eliminado de manera satisfactoria!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
