﻿using FrmNotePad.Secuencial;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrmNotePad
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void NuevoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            IFrmNotePad ifnp = new IFrmNotePad();
            //ifnp.MdiChildren = this;
            ifnp.Show();
        }

        private void AbrirToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            string filepath = openFileDialog1.FileName;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SecuencialStream ss = new SecuencialStream(filepath);
                string text = ss.ReadText();
                IFrmNotePad ifnp = new IFrmNotePad();
                ifnp.Text = openFileDialog1.SafeFileName;
                //ifnp.textBox.Text = text;
                ifnp.Show();
            }
        }

        private void GuardarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            int count = this.MdiChildren.Length;
            if(count == 0)
            {
                return;
            }
            DialogResult result = saveFileDialog1.ShowDialog();
            if(result == DialogResult.OK)
            {
                string filepath = saveFileDialog1.FileName;
                SecuencialStream ss = new SecuencialStream(filepath);
                Form activeChild = this.ActiveMdiChild;
                TextBox txtarea = (TextBox)activeChild.Controls[0];
                ss.WriteText(txtarea.Text);
            }
        }

        private void SalirToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
